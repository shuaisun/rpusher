var util = require('util'),
    http = require('http');
	dialog = require('dialog');
	sock = require('socket.io');
	coffee = require('coffee-script');

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/plain'});
  res.write('hello, i know nodejitsu.')
  res.end();
}).listen(8888);

/* server started */  
util.puts('> hello world running on port 8888');

setTimeout(function () {
  util.puts('Hello there.');
  dialog.info('Hello there');
  throw new Error('We need to stop in here.');
}, 5000);